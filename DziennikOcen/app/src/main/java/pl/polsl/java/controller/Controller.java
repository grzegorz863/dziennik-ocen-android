package pl.polsl.java.controller;

import java.io.Serializable;
import pl.polsl.java.model.RatingLog;
import pl.polsl.java.model.Student;
import pl.polsl.java.exception.StudentNotFoundException;

/**
 * This class is responsible for redading and save files, controll input data
 * and transfer informations between model and view
 *
 * @author Grzegorz Nowak
 * @version 1.2
 */
public class Controller implements Serializable {

    /**
     * RatingLog class object.
     */
    private RatingLog ratingLog = new RatingLog();

    /**
     * Constructor.
     */
    public Controller() {   }

    /**
     * Transmit informations obot ratings of selected student.
     */
    public String showAllStudentRatings(String name, String surname) throws StudentNotFoundException {
        String ratings = ratingLog.allRatingsOfStudent(name, surname);
        return ratings;

    }

    /**
     * Adds studets to rating log.
     */
    public void addStudent(String name, String surname) {
        Student newStudent = new Student(name,surname);
        ratingLog.addStudent(newStudent);
    }

    /**
     * Adds rate to selected student.
     */
    public void addRateToSelectedStudent(String name, String surname, String rate) throws StudentNotFoundException, NumberFormatException{
        double rate_d = Double.parseDouble(rate);
        ratingLog.addRateToStudent(name, surname, rate_d);
    }
}
