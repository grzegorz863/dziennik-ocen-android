package pl.polsl.java.proj;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import pl.polsl.java.controller.Controller;


/**
 * Class represents activity to adding students.
 *
 * @author Grzegorz Nowak
 * @version 1.0
 */
public class AddStudentActivity extends AppCompatActivity {

    /**
     * Field to enter name
     */
    private EditText ETname;

    /**
     * Field to enter surname
     */
    private EditText ETsurname;


    /**
     * Method called during creation this activity
     *
     * @param savedInstanceState - saved instance state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);

        ETname = findViewById(R.id.ETAddStudentName);
        ETsurname = findViewById(R.id.ETAddStudentSurname);
    }

    /**
     * Method called after click button "ADD STUDENT" in this activity, adds student to rating log.
     *
     * @param v - view
     */
    public void onClickAdd(View v){
        String name = ETname.getText().toString();
        String surname = ETsurname.getText().toString();

        Controller controller = (Controller) getIntent().getSerializableExtra("controller");

        if((!name.isEmpty()) && (!surname.isEmpty())) {
            Toast.makeText(getApplicationContext(), "Added student: "+name+" "+surname, Toast.LENGTH_SHORT).show();
            controller.addStudent(name, surname);
            Intent intent = getIntent();
            intent.putExtra("controller", controller);
            setResult(RESULT_OK, intent);
        }else{
            Toast.makeText(getApplicationContext(), "Fill in all fields!", Toast.LENGTH_SHORT).show();
        }
    }
}
