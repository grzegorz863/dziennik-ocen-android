package pl.polsl.java.exception;

/**
 * This class extends Exception class, it is throwing when student is not found.
 * @author Grzegorz Nowak
 * @version 1.0
 */
public class StudentNotFoundException extends Exception {
    /**
     * Exception message.
     */
    String message;

    /**
     * Constructor.
     * @param message Message of exception.
     */
    public StudentNotFoundException(String message) {
        this.message = message;
    }

    /**
     * This is override method, returns exception message.
     * @return Exception message.
     */
    @Override
    public String getMessage() {
        return this.message;
    }
}
