package pl.polsl.java.proj;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import pl.polsl.java.controller.Controller;
import pl.polsl.java.exception.StudentNotFoundException;

/**
 * Class represents activity to adding rates.
 *
 * @author Grzegorz Nowak
 * @version 1.0
 */
public class AddRateActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * Field to enter name
     */
    private EditText ETname;

    /**
     * Field to enter surname
     */
    private EditText ETsurname;

    /**
     * Field to enter rate
     */
    private EditText ETrate;

    /**
     * Button to click
     */
    private Button BAddRate;

    /**
     * Method called during creation this activity
     *
     * @param savedInstanceState - saved instance state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_rate);

        ETname = findViewById(R.id.ETAddRateName);
        ETsurname = findViewById(R.id.ETAddRateSurname);
        ETrate = findViewById(R.id.ETAddRateRate);
        BAddRate = findViewById(R.id.BAddRate);
        BAddRate.setOnClickListener(this);
    }

    /**
     * Method called after click button "ADD RATE" in this activity, displays rates.
     *
     * @param v - view
     */
    public void onClick(View v){
        String name = ETname.getText().toString();
        String surname = ETsurname.getText().toString();
        String rate = ETrate.getText().toString();
        Controller controller = (Controller) getIntent().getSerializableExtra("controller");

        if((!name.isEmpty()) && (!surname.isEmpty()) && (!rate.isEmpty())) {
            try {
                controller.addRateToSelectedStudent(name, surname, rate);
                Toast.makeText(getApplicationContext(), "Added rate: "+rate+" to "+name+" "+surname, Toast.LENGTH_SHORT).show();
                Intent intent = getIntent();
                intent.putExtra("controller", controller);
                setResult(RESULT_OK, intent);
            }catch (StudentNotFoundException e){
                Toast.makeText(getApplicationContext(),e.getMessage(), Toast.LENGTH_SHORT).show();
            }catch (NumberFormatException e){
                Toast.makeText(getApplicationContext(),"Wrong number format or rate format!", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(getApplicationContext(), "Fill in all fields!", Toast.LENGTH_SHORT).show();
        }

    }
}
