package pl.polsl.java.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representing the student, has his name, surname and list with all
 * ratings.
 *
 * @author Grzegorz Nowak
 * @version 1.1
 */
public class Student implements Serializable{

    /**
     * Student's name.
     */
    private String name;

    /**
     * Student's surname.
     */
    private String surname;

    /**
     * List of student's ratings.
     */
    private List<Rate> ratings = new ArrayList<>();

    /**
     * Constructor.
     *
     * @param _name Student's name.
     * @param _surname Student's surname.
     */
    public Student(String _name, String _surname) {

        this.name = _name;
        this.surname = _surname;
        this.ratings = new ArrayList<>();
    }

    /**
     * This is override method, returns String with name, surname, and all
     * student's ratings.
     *
     * @return String with name, surname, and all student's ratings.
     */
    @Override
    public String toString() {
        String student;
        student = this.name + " " + this.surname;
        for (Rate r : ratings) {
            student += " " + String.valueOf(r.valueOfRate());
        }
        return student;
    }

    /**
     * Adds rate to list of ratings.
     *
     * @param rate student's rate
     */
    public void addRate(Rate rate) throws NullPointerException {
        if ((rate.valueOfRate() >= 2) && (rate.valueOfRate() <= 5)) {
            ratings.add(rate);
        }
    }

    /**
     * Returns student's name and student's surname in single String.
     *
     * @return student's name and student's surname in single String.
     */
    public String nameAndSurname() {
        return this.name + this.surname;
    }
}
