package pl.polsl.java.proj;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import pl.polsl.java.controller.*;

/**
 * Class represents main view with 3 buttons.
 *
 * @author Grzegorz Nowak
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity{

    /**
     * Declaration of Controller
     */
    private Controller controller;

    /**
     * Method called during creation main activity
     *
     * @param savedInstanceState - saved instance state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        controller = new Controller();
    }

    /**
     * Method called after click button "SHOW ALL STUDENTS RATINGS"
     *
     * @param v - view
     */
    public void onClickShow (View v){
        Intent intent = new Intent(this,ShowRatingsActivity.class);
        intent.putExtra("controller", controller);
        startActivity(intent);
    }

    /**
     * Method called after click button "ADD A STUDENT"
     *
     * @param v - view
     */
    public void onClickAddStudent (View v){
        Intent intent = new Intent(this,AddStudentActivity.class);
        intent.putExtra("controller", controller);
        startActivityForResult(intent, 0);
    }

    /**
     * Method called after click button "ADD RATE TO SELECTED STUDENT"
     *
     * @param v - view
     */
    public void onClickAddRate (View v){
        Intent intent = new Intent(this,AddRateActivity.class);
        intent.putExtra("controller", controller);
        startActivityForResult(intent, 1);
    }

    /**
     * Method called after return from subactivity (add student and add rate)
     *
     * @param requestCode - request number
     * @param resultCode - result status
     * @param data - data from subactivity
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case 0:
                    controller = (Controller) data.getSerializableExtra("controller");
                    break;
                case 1:
                    controller = (Controller) data.getSerializableExtra("controller");
                    break;
            }
        }
    }
}
