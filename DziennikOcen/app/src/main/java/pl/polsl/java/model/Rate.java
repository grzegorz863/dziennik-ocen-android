package pl.polsl.java.model;

import java.io.Serializable;

/**
 * Supports single rate
 *
 * @author Grzegorz Nowak
 * @version 1.0
 */
public class Rate implements Serializable{

    /**
     * value of rate
     */
    private double value;

    /**
     * Constructor
     *
     * @param r value of rate
     */
    public Rate(double r) {
        this.value = r;
    }

    /**
     * Returns value of rate
     *
     * @return value of rate
     */
    public double valueOfRate() {
        return this.value;
    }
}
