package pl.polsl.java.proj;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import pl.polsl.java.controller.Controller;
import pl.polsl.java.exception.StudentNotFoundException;

/**
 * Class represents activity to showing rates.
 *
 * @author Grzegorz Nowak
 * @version 1.0
 */
public class ShowRatingsActivity extends AppCompatActivity {

    /**
     * Field to enter name
     */
    private EditText ETname;

    /**
     * Field to enter surname
     */
    private EditText ETsurname;

    /**
     * Field to displaying rates
     */
    private TextView ETrates;

    /**
     * Method called during creation this activity
     *
     * @param savedInstanceState - saved instance state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_ratings);

        ETname = findViewById(R.id.ETShowRatingsName);
        ETsurname = findViewById(R.id.ETShowRatingsSurname);
        ETrates = findViewById(R.id.textViewWithRates);
    }

    /**
     * Method called after click button "SHOW RATINGS" in this activity, displays rates.
     *
     * @param v - view
     */
    public void onClickShow(View v){
        String name = ETname.getText().toString();
        String surname = ETsurname.getText().toString();

        Controller controller = (Controller) getIntent().getSerializableExtra("controller");

        if((!name.isEmpty()) && (!surname.isEmpty())) {
            try {
                String rates = controller.showAllStudentRatings(name, surname);
                ETrates.setText(rates);
            }catch (StudentNotFoundException e){
                Toast.makeText(getApplicationContext(),e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(getApplicationContext(), "Fill in all fields!", Toast.LENGTH_SHORT).show();
        }
    }
}
