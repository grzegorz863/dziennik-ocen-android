package pl.polsl.java.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import pl.polsl.java.exception.StudentNotFoundException;

/**
 * Rating Log with list of students and methods allows to operate on them.
 *
 * @author Grzegorz Nowak
 * @version 1.2
 */
public class RatingLog implements Serializable{

    /**
     * List of students.
     */
    private List<Student> students = new ArrayList<>();

    /**
     * Finds student in list od students.
     *
     * @param name Student's name.
     * @param surname Student's surname.
     * @throws StudentNotFoundException is thrown when a student is not found
     * @return Found student.
     */
    private Student findStudent(String name, String surname) throws StudentNotFoundException {
        for (Student s : students) {
            if (s.nameAndSurname().equals(name + surname)) {
                return s;
            }

        }

        throw new StudentNotFoundException("Student not fount");
    }

    /**
     * Adds student to list.
     *
     * @param s student.
     */
    public void addStudent(Student s) {
        if (s != null && !s.nameAndSurname().equals("")) {
            students.add(s);
        }
    }

    /**
     * Adds rate to selecred student.
     *
     * @param name Student's name.
     * @param surname Student's surname.
     * @param rate Student's rate.
     * @throws StudentNotFoundException is thrown when a student is not found.
     */
    public void addRateToStudent(String name, String surname, double rate) throws StudentNotFoundException, NumberFormatException {
        try {
            if ((rate >= 2) && (rate <= 5)) {
                Rate r = new Rate(rate);
                this.findStudent(name, surname).addRate(r);
            }
            else{
                throw new NumberFormatException();
            }
        } catch (NullPointerException error) {
        }
    }

    /**
     * Returns String with all ratings of selected student.
     *
     * @param name Student's name.
     * @param surname Student's surname.
     * @return All ratings of selected student.
     * @throws StudentNotFoundException is thrown when a student is not found.
     */
    public String allRatingsOfStudent(String name, String surname) throws StudentNotFoundException {
        return this.findStudent(name, surname).toString();
    }
}
